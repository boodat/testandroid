import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import SelectDropdown from 'react-native-select-dropdown';
import Icon from 'react-native-vector-icons/Ionicons';

function icon() {
  return <Icon name="chevron-down" color={'#000'} size={25} />;
}
const Pendaftaran = () => {

  const tipeSekolah = ['Negeri', 'Swasta'];
  const provinsi = ['tipe A', 'tipe B', 'tipe C', 'tipe D'];
  const kotaKabupaten = [
    'Kabupaten A',
    'Kabupaten B',
    'Kabupaten C',
    'Kota A',
    'Kota B',
  ];

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.content}>
          <View
            style={{
              width: '100%',
              height: 40,
              backgroundColor: '#CDDEF2',
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderTopRightRadius: 7,
              borderTopLeftRadius: 7,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                textDecorationLine: 'underline',
                fontSize: 14,
                fontWeight: '600',
                color: '#000',
              }}>
              Data Sekolah :
            </Text>
          </View>

          <View style={styles.contentField}>
            <Text style={styles.labelText}>Tipe Sekolah : *</Text>
            <SelectDropdown
              data={tipeSekolah}
              onSelect={(selectedItem, index) => {
                console.log(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem;
              }}
              rowTextForSelection={(item, index) => {
                return item;
              }}
              defaultButtonText={'Pilih tipe'}
              buttonStyle={styles.dropDownBtn}
              selectedRowTextStyle={{
                color: 'blue',
              }}
              buttonTextStyle={{
                fontSize: 14,
                textAlign: 'auto',
                color: '#020202',
              }}
              renderDropdownIcon={icon}
              dropdownIconPosition="right"
              rowStyle={{
                height: 35,
                marginVertical: 2,
                borderRadius: 20,
              }}
              rowTextStyle={{
                textAlign: 'auto',
                fontSize: 14,
                marginHorizontal: 20,
              }}
              dropdownStyle={{borderRadius: 5}}
            />

            <Text style={styles.labelText}>Nama Sekolah : *</Text>
            <TextInput
              placeholder="Contoh SMK Negri A Dimanapun"
              style={styles.textInput}
            />

            <Text style={styles.labelText}>Alamat : *</Text>
            <TextInput style={styles.textInput} />
            <Text style={styles.labelText}>Kode Pos : *</Text>
            <TextInput style={styles.textInput}
            keyboardType='number-pad'
            maxLength={5}
             />
             
            <Text style={styles.labelText}>Provinsi : *</Text>
            <SelectDropdown
              data={provinsi}
              onSelect={(selectedItem, index) => {
                console.log(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem;
              }}
              rowTextForSelection={(item, index) => {
                return item;
              }}
              defaultButtonText={'Pilih provinsi'}
              buttonStyle={styles.dropDownBtn}
              selectedRowTextStyle={{
                color: 'blue',
              }}
              buttonTextStyle={{
                fontSize: 14,
                textAlign: 'auto',
                color: '#020202',
              }}
              renderDropdownIcon={icon}
              dropdownIconPosition="right"
              rowStyle={{
                height: 35,
                marginVertical: 2,
                borderRadius: 20,
              }}
              rowTextStyle={{
                textAlign: 'auto',
                fontSize: 14,
                marginHorizontal: 20,
              }}
              dropdownStyle={{borderRadius: 5}}
            />

            <Text style={styles.labelText}>Kota/Kabupaten : *</Text>
            <SelectDropdown
              data={kotaKabupaten}
              onSelect={(selectedItem, index) => {
                console.log(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem;
              }}
              rowTextForSelection={(item, index) => {
                return item;
              }}
              defaultButtonText={'Pilih kota/kabupaten'}
              buttonStyle={styles.dropDownBtn}
              selectedRowTextStyle={{
                color: 'blue',
              }}
              buttonTextStyle={{
                fontSize: 14,
                textAlign: 'auto',
                color: '#020202',
              }}
              renderDropdownIcon={icon}
              dropdownIconPosition="right"
              rowStyle={{
                height: 35,
                marginVertical: 2,
                borderRadius: 20,
              }}
              rowTextStyle={{
                textAlign: 'auto',
                fontSize: 14,
                marginHorizontal: 20,
              }}
              dropdownStyle={{borderRadius: 5}}
            />

            <Text style={styles.labelText}>No Telepon Sekolah : *</Text>
            <TextInput style={styles.textInput} />
            <Text style={styles.labelText}>Email Sekolah : *</Text>
            <TextInput style={styles.textInput} />
            <Text style={styles.labelText}>Facebook : </Text>
            <TextInput style={styles.textInput} />
            <Text style={styles.labelText}>Jumlah Siswa : *</Text>
            <TextInput style={styles.textInput} />
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Pendaftaran;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {},
  contentField: {
    paddingHorizontal: 15,
    marginVertical: 10,
  },
  textInput: {
    width: '100%',
    height: 35,
    backgroundColor: '#fff',
    borderColor: '#EAEAEA',
    borderWidth: 1,
    borderRadius: 5,
    marginVertical: 5,
    fontSize: 13,
    color: '#020202',
    paddingHorizontal: 10,
  },
  labelText: {
    fontSize: 14,
    color: 'black',
    marginTop: 10,
    paddingHorizontal: 5,
  },
  dropDownBtn: {
    width: '100%',
    height: 35,
    backgroundColor: '#fff',
    borderColor: '#EAEAEA',
    borderWidth: 1,
    borderRadius: 5,
    marginVertical: 5,
  },
});

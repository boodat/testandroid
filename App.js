import React from 'react'
import Apps from './app/index'
import { SafeAreaView, View, Text } from 'react-native'


const App = () => {
  return(
    <SafeAreaView>
      <View>
        <Apps />
      </View>
    </SafeAreaView>
  )
}

export default App;